process cnvkit_batch {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'cnvkit_container'
  label 'cnvkit_batch'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/cnvkit_batch"

  input:
    tuple val(pat_name), val(dataset), val(norm_run), path(norm_bam), path(norm_bai), val(tumor_run), path(tumor_bam), path(tumor_bai)
    path targets
    path fa
    val parstr
  output:
    tuple val(pat_name), val(dataset), val(norm_run), val(tumor_run), path("*cnvkit.outputs"), emit: cnvkit_outputs
  script:
  """
  cnvkit.py batch *-ad*bam --normal *-nd*bam \
      --targets ${targets} \
      --fasta ${fa} \
      --output-reference joint_normals.cnn \
      --output-dir ${dataset}-${pat_name}-${norm_run}_${tumor_run}.cnvkit.outputs \
      ${parstr}
  """
}

process cnvkit_guess_baits {

  tag "${bed}"
  label 'cnvkit_container'
  label 'cnvkit_guess_baits'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/cnvkit_guess_baits"

  input:
  path bams
  path bed

  output:
  path "baits.bed", emit: baits_bed
  path "baits.3col.bed", emit: baits_3col_bed

  script:
  def bed_proxy  = bed.name != 'dummy_file' ? "-t ${bed}" : ''
  """
  guess_baits.py *bam ${bed_proxy} -o baits.bed
  cut -f 1-3 baits.bed > baits.3col.bed
  """
}

process cnvkit_autobin {

  tag "${bed}"
  label 'cnvkit_container'
  label 'cnvkit_autobin'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/cnvkit_autobin"

  input:
  path bams
  path gtf
  path bed
  val parstr

  output:
  path "targets.bed", emit: targets_bed
  path "antitargets.bed", emit: antitargets_bed
  tuple path("targets.bed"), path("antitargets.bed"), emit: targets_and_antitargets_beds

  script:
  annotate_proxy = gtf.name != 'dummy_file' ? "--annotate ${gtf}" : ''
  """
  cnvkit.py autobin *.bam \
  ${parstr} \
  -t ${bed} \
  ${annotate_proxy} \
  --target-output-bed targets.bed \
  --antitarget-output-bed antitargets.bed
  """
}

process cnvkit_coverage {

  tag "${dataset}/${pat_name}/${run}"
  label 'cnvkit_container'
  label 'cnvkit_coverage'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/cnvkit_coverage"

  input:
  tuple val(pat_name), val(run), val(dataset),path(bam)
  path bed
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.*targetscoverage.cnn"), emit: cnns

  script:
  """
  BED=`echo ${bed} | cut -f 1 -d '.'`
  cnvkit.py coverage \
  ${bam} \
  ${bed} \
  -o ${dataset}-${pat_name}-${run}.\${BED}coverage.cnn \
  ${parstr}
  """
}

process cnvkit_reference {

  label 'cnvkit_container'
  label 'cnvkit_reference'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/cnvkit_reference"

  input:
  path targets_cnns
  path antiargets_cnns
  path fa
  val parstr

  output:
  path("reference.cnn"), emit: reference_cnn

  script:
  """
  cnvkit.py reference \
  *coverage.cnn \
  -f ${fa} \
  -o reference.cnn \
  ${parstr}
  """
}

process cnvkit_fix {

  tag "${dataset}/${pat_name}/${run}"
  label 'cnvkit_container'
  label 'cnvkit_fix'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/cnvkit_fix"

  input:
  tuple val(pat_name), val(run), val(dataset), path(targets_cnn), path(antitargets_cnn)
  path reference_cnn
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.cnr"), emit: cnrs

  script:
  """
  cnvkit.py fix \
  -i ${dataset}-${pat_name}-${run} \
  ${targets_cnn} \
  ${antitargets_cnn} \
  ${reference_cnn} \
  -o ${dataset}-${pat_name}-${run}.cnr \
  ${parstr}
  """
}

process cnvkit_segment {

  tag "${dataset}/${pat_name}/${run}"
  label 'cnvkit_container'
  label 'cnvkit_segment'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/cnvkit_segment"

  input:
  tuple val(pat_name), val(run), val(dataset), path(cnr)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.cns"), emit: cnss

  script:
  """
  cnvkit.py segment \
  ${cnr} \
  -o ${dataset}-${pat_name}-${run}.cns \
  ${parstr}
  """
}

process cnvkit_call {

  tag "${dataset}/${pat_name}/${run}"
  label 'cnvkit_container'
  label 'cnvkit_call'
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/cnvkit_call"

  input:
  tuple val(pat_name), val(run), val(dataset), path(vcf), path(cns)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.call.cns"), emit: call_cnss

  script:
  def vcf_proxy = vcf.name != 'dummy_file' ? "-v ${vcf}" : ''
  """
  cnvkit.py call \
  ${cns} \
  ${vcf_proxy} \
  -o ${dataset}-${pat_name}-${run}.call.cns \
  ${parstr}
  """
}

process cnvkit_scatter {

  tag "${dataset}/${pat_name}/${run}"
  label 'cnvkit_container'
  label 'cnvkit_scatter'
  cache 'lenient'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/cnvkit_scatter"

  input:
  tuple val(pat_name), val(run), val(dataset), path(cnr_cns)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.pdf"), emit: scatter_pdfs

  script:
  def cnr_cns_proxy = cnr_cns.name =~ /.cns/ ? "-s ${cnr_cns}" : "${cnr_cns}"
  """
  SUFFIX=`echo ${cnr_cns} | cut -f 2 -d '.'`
  cnvkit.py scatter \
  ${cnr_cns_proxy} \
  -o ${dataset}-${pat_name}-${run}.scatter.\${SUFFIX}.pdf \
  ${parstr}
  """
}

process cnvkit_scatter_cns_cnr {

  tag "${dataset}/${pat_name}/${run}"
  label 'cnvkit_container'
  label 'cnvkit_scatter'
  cache 'lenient'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/cnvkit_scatter"

  input:
  tuple val(pat_name), val(run), val(dataset), path(cns), path(cnr)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.pdf"), emit: scatter_pdfs

  script:
  """
  cnvkit.py scatter \
  ${cnr} \
  -s ${cns} \
  -o ${dataset}-${pat_name}-${run}.scatter.cns_cnr.pdf \
  ${parstr}
  """
}
